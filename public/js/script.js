var btnEnviar = document.getElementById("idBtnEnviar")
btnEnviar.onclick = async function () {
    var horaAvaliacao = timestamp()
    
    var avaliacao = {
        a: document.querySelector("input[name=nmA]:checked").value,
        b: document.querySelector("input[name=nmB]:checked").value,
        c: document.querySelector("input[name=nmC]:checked").value,
        d: document.querySelector("#idNota").value,
        e: document.querySelector("#idIdade").value,
        f: document.querySelector("input[name=nmF]:checked").value,
        timestamp: horaAvaliacao
    }

    
    var aut = localStorage.getItem("token")
    //console.log(aut);

    

    var options = {
        method: 'POST',
        headers: {'Content-Type': 'application/json', 'x-access-token': aut},
        body: JSON.stringify(avaliacao)
    }
    
    var response = await fetch(`http://localhost:3002/pesquisa`, options)
    response = await response.json()

    var outResposta = document.getElementById("idOut")

    if (response.status == '200'){
        outResposta.value = response.mensagem
        setTimeout(() => {
            outResposta.style.display = "none"

        }, 2000);
        
        outResposta.style.display = "block"
    } 
    else {
        outResposta.value = response.mensagem
        outResposta.style.color="red"



    }
    

}


function timestamp() {
let data = new Date();

let ano = data.getFullYear();

var mes = (data.getMonth() + 1)
if (mes < 10) {
    mes = String("0" + mes)

}

var dia = data.getDate()
if (dia < 10) {
    dia = String("0" + dia)
}

var hora = data.getHours();
if (hora < 10) {
    hora = String("0" + hora)
}

var minuto = data.getMinutes();
if (minuto < 10) {
    minuto = String("0" + minuto)
}

var segundo = data.getSeconds();
if (segundo < 10) {
    segundo = String("0" + segundo)
}

return ano + mes + dia + hora + minuto + segundo
}

