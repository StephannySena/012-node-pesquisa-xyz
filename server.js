const express = require("express")
const cors = require("cors")
const app = express()
const path = require("path")
const fs = require("fs")
const jwt = require("jsonwebtoken")
const SECRET = "SohEuSei"

const handlebars = require("express-handlebars")


const porta = 3002

app.use(cors())
app.use(express.urlencoded({ extended: false }))
app.use(express.json())

//configurar handlebars
app.engine("handlebars", handlebars({defaultLayout: "main"}))
app.set("view engine", "handlebars")

app.listen(porta, function () {
    console.log(`Servidor rodando na porta ${porta}`);
})

app.use(express.static(path.join(__dirname, "public")));

function verifyJWT(req, resp, next) {
    const token = req.header("x-access-token")
    //console.log(token);
    jwt.verify(token, SECRET, function (err, decoded) {
        if (err) {
            resp.status(401).end()
        }
        //req.dec = decoded.user
        next()
    })
}



app.get("/", function (req, resp) {
    resp.sendFile(__dirname + "/view/index.html")
})

app.post("/pesquisa", verifyJWT, function (req, resp) {

    let avaliacao = `${req.body.a},${req.body.b},${req.body.c},${req.body.d},${req.body.e},${req.body.f},${req.body.timestamp}`

    fs.appendFile("pesquisa.csv", `${avaliacao}\n`, function (err) {
        if (err) {
            resp.json({
                "status": "500",
                "mensagem": err
            })

        }
        else {
            resp.json({
                "status": "200",
                "mensagem": "Pesquisa Registrada Com sucesso"
            })
        }
    })


})

app.get("/estatisticas", function (req, resp) {
    var avaliacoes = []
    fs.readFile("pesquisa.csv", "utf8", function (err, data) {
        var avaliacao = data.split("\n")
        avaliacao.forEach(element => {
            avaliacoes.push(element.split(","))
        })

        var estatisticas = calculoEstatisticas(avaliacoes)
        resp.json(estatisticas)
    })


})

app.get("/relatorio", function (req, resp,) {
    var avaliacoes = []
    fs.readFile("pesquisa.csv", "utf8", function (err, data) {
        var avaliacao = data.split("\n")
        avaliacao.forEach(element => {
            avaliacoes.push(element.split(","))
        })

        var estatisticas = calculoEstatisticas(avaliacoes)

        resp.send(

            `
                <!DOCTYPE html>
                <html lang="en">
                <head>
                    <meta charset="UTF-8">
                    <meta http-equiv="X-UA-Compatible" content="IE=edge">
                    <meta name="viewport" content="width=device-width, initial-scale=1.0">
                    <title>Relatório</title>
                    <link rel="stylesheet" href="http://localhost:3002/css/estilos.css">

                </head>
                <body>
                    <h1>Relatório Pesquisa XYZ</h1>
                
                    <div class="clQuestao">
                        <p>Questão A: Sua solicitação foi atendida?</p>
                        <p>Não foi atendida: ${estatisticas.a.naoAtendida}%</p>
                        <p>Parcialmente atendida: ${estatisticas.a.parcAtendida}%</p>
                        <p>Totalmente Atendida: ${estatisticas.a.atendida}%</p>
                    </div>
                
                    <div class="clQuestao">
                        <p>Questão B: Qual nota você daria para o atendimento? </p>
                        <p>Péssimo: ${estatisticas.b.pessimo}%</p>
                        <p>Ruim: ${estatisticas.b.ruim}%</p>
                        <p>Aceitável: ${estatisticas.b.aceitavel}%</p>
                        <p>Bom: ${estatisticas.b.bom}%</p>
                        <p>Excelente: ${estatisticas.b.excelente}%</p>
                    </div>
                
                    <div class="clQuestao">
                        <p>Questão C: Como você classificaria o comportamento do atendente?</p>
                        <p>Indelicado: ${estatisticas.c.indelicado}%</p>
                        <p>Mau Humorado: ${estatisticas.c.mauHumorado}%</p>
                        <p>Neutro: ${estatisticas.c.neutro}%</p>
                        <p>Educado: ${estatisticas.c.educado}%</p>
                        <p>Muito Atencioso: ${estatisticas.c.muitoAtencioso}%</p>
                    </div>
                    
                    <div class="clQuestao">
                        <p>Questão D: De 0 à 10, qual nota você daria para o produto </p>
                        <p>0: ${estatisticas.d.zero}%</p>
                        <p>1: ${estatisticas.d.um}%</p>
                        <p>2: ${estatisticas.d.dois}%</p>
                        <p>3: ${estatisticas.d.tres}%</p>
                        <p>4: ${estatisticas.d.quatro}%</p>
                        <p>5: ${estatisticas.d.cinco}%</p>
                        <p>6: ${estatisticas.d.seis}%</p>
                        <p>7: ${estatisticas.d.sete}%</p>
                        <p>8: ${estatisticas.d.oito}%</p>
                        <p>9: ${estatisticas.d.nove}%</p>
                        <p>10: ${estatisticas.d.dez}%</p>
                    </div>

                    <div class="clQuestao">
                        <p>Questão E: Informe sua idade: </p>
                        <p>Menos de 15: ${estatisticas.e.menosDe15}%</p>
                        <p>De 15 a 21: ${estatisticas.e.de15a21}%</p>
                        <p>De 22 a 35: ${estatisticas.e.de22a35}%</p>
                        <p>De 36 a 50: ${estatisticas.e.de36a50}%</p>
                        <p>Mais de 50: ${estatisticas.e.maisde50}%</p>
                    </div>

                    <div class="clQuestao">
                        <p>Questão F: Informe seu gênero: </p>
                        <p>Feminino: ${estatisticas.f.feminino}%</p>
                        <p>Masculino: ${estatisticas.f.masculino}%</p>
                        <p>Outro: ${estatisticas.f.outro}%</p>
                    </div>
                    
                </body>
                </html>
            
            
            `

        )
    })
})

app.get("/login", function (req, resp) {
    resp.sendFile(__dirname + "/view/login.html")

})


app.post("/login", function (req, resp,) {
    fs.readFile("usuarios.csv", "utf8", function (err, data) {
        var dadosAcesso = []
        var dadoAcesso = data.split("\r\n")
        dadoAcesso.forEach(element => {
            dadosAcesso.push(element.split(","))
        })
        //console.log(dadosAcesso);

        var resposta = autenticacao(req.body.user, req.body.pass, dadosAcesso)
        if (resposta) {
            //gerar token 
            const token = jwt.sign({ user: req.body.user }, SECRET, { expiresIn: 300 })
            resp.json({ auth: true, token })
        }
        resp.status(401).end()
    })

})



app.get("/dados", function (req, resp) {
    var avaliacoes = []
    fs.readFile("pesquisa.csv", "utf8", function (err, data) {
        var avaliacao = data.split("\n")
        avaliacao.forEach(element => {
            avaliacoes.push(element.split(","))
        })

        var estatisticas = calculoEstatisticas(avaliacoes)
        
        resp.render("relatorio", estatisticas)
    })

})


function autenticacao(userId, senha, dadosAcesso) {
    for (let i = 0; i < dadosAcesso.length; i++) {
        if (userId == dadosAcesso[i][0] && senha === dadosAcesso[i][1]) {

            return true
        }
    }
    return false
}


function calculoEstatisticas(avaliacoes) {


    var questaoA = [0, 0, 0]
    var questaoB = [0, 0, 0, 0, 0]
    var questaoC = [0, 0, 0, 0, 0]
    var questaoD = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    var questaoE = [0, 0, 0, 0, -1] // -1 ref a linha em branco no pesquisa.csv
    var questaoF = [0, 0, 0]
    var totalRespostas

    for (let i = 0; i < avaliacoes.length; i++) {
        totalRespostas = i

        //QUESTAO A
        switch (avaliacoes[i][0]) {
            case "NaoAtendida":
                questaoA[0]++
                break;

            case "ParcAtendida":
                questaoA[1]++
                break;

            case "Atendida":
                questaoA[2]++
                break;
        }

        //QUESTAO B
        switch (avaliacoes[i][1]) {
            case "Pessimo":
                questaoB[0]++
                break;

            case "Ruim":
                questaoB[1]++
                break;

            case "Aceitavel":
                questaoB[2]++
                break;

            case "Bom":
                questaoB[3]++
                break;

            case "Excelente":
                questaoB[4]++
                break;
        }

        //QUESTAO C
        switch (avaliacoes[i][2]) {
            case "indelicado":
                questaoC[0]++
                break;

            case "mauHumorado":
                questaoC[1]++
                break;

            case "neutro":
                questaoC[2]++
                break;

            case "educado":
                questaoC[3]++
                break;

            case "muitoAtencioso":
                questaoC[4]++
                break;
        }

        //QUESTAO D
        switch (avaliacoes[i][3]) {
            case "0":
                questaoD[0]++
                break;

            case "1":
                questaoD[1]++
                break;

            case "2":
                questaoD[2]++
                break;

            case "3":
                questaoD[3]++
                break;

            case "4":
                questaoD[4]++
                break;

            case "5":
                questaoD[5]++
                break;

            case "6":
                questaoD[6]++
                break;

            case "7":
                questaoD[7]++
                break;

            case "8":
                questaoD[8]++
                break;

            case "9":
                questaoD[9]++
                break;

            case "10":
                questaoD[10]++
                break;
        }

        //QUESTAO E
        let idade = Number(avaliacoes[i][4])
        if (idade < 15) {
            questaoE[0]++
        }
        else if (idade >= 15 && idade <= 21) {
            questaoE[1]++
        }
        else if (idade >= 22 && idade <= 35) {
            questaoE[2]++
        }
        else if (idade >= 36 && idade <= 50) {
            questaoE[3]++
        }
        else {
            questaoE[4]++
        }

        //QUESTAO F
        switch (avaliacoes[i][5]) {
            case "Feminino":
                questaoF[0]++
                break;

            case "Masculino":
                questaoF[1]++
                break;

            case "Outro":
                questaoF[2]++
                break;
        }

    }

    let estatisticas = {
        a: {
            naoAtendida: calcularPorcentagem(questaoA[0], totalRespostas),
            parcAtendida: calcularPorcentagem(questaoA[1], totalRespostas),
            atendida: calcularPorcentagem(questaoA[2], totalRespostas),
        },

        b: {
            pessimo: calcularPorcentagem(questaoB[0], totalRespostas),
            ruim: calcularPorcentagem(questaoB[1], totalRespostas),
            aceitavel: calcularPorcentagem(questaoB[2], totalRespostas),
            bom: calcularPorcentagem(questaoB[3], totalRespostas),
            excelente: calcularPorcentagem(questaoB[4], totalRespostas)

        },

        c: {
            indelicado: calcularPorcentagem(questaoC[0], totalRespostas),
            mauHumorado: calcularPorcentagem(questaoC[1], totalRespostas),
            neutro: calcularPorcentagem(questaoC[2], totalRespostas),
            educado: calcularPorcentagem(questaoC[3], totalRespostas),
            muitoAtencioso: calcularPorcentagem(questaoC[4], totalRespostas)

        },

        d: {
            zero: calcularPorcentagem(questaoD[0], totalRespostas),
            um: calcularPorcentagem(questaoD[1], totalRespostas),
            dois: calcularPorcentagem(questaoD[2], totalRespostas),
            tres: calcularPorcentagem(questaoD[3], totalRespostas),
            quatro: calcularPorcentagem(questaoD[4], totalRespostas),
            cinco: calcularPorcentagem(questaoD[5], totalRespostas),
            seis: calcularPorcentagem(questaoD[6], totalRespostas),
            sete: calcularPorcentagem(questaoD[7], totalRespostas),
            oito: calcularPorcentagem(questaoD[8], totalRespostas),
            nove: calcularPorcentagem(questaoD[9], totalRespostas),
            dez: calcularPorcentagem(questaoD[10], totalRespostas),

        },

        e: {
            menosDe15: calcularPorcentagem(questaoE[0], totalRespostas),
            de15a21: calcularPorcentagem(questaoE[1], totalRespostas),
            de22a35: calcularPorcentagem(questaoE[2], totalRespostas),
            de36a50: calcularPorcentagem(questaoE[3], totalRespostas),
            maisde50: calcularPorcentagem(questaoE[4], totalRespostas),

        },

        f: {
            feminino: calcularPorcentagem(questaoF[0], totalRespostas),
            masculino: calcularPorcentagem(questaoF[1], totalRespostas),
            outro: calcularPorcentagem(questaoF[2], totalRespostas),
        }
    }

    return estatisticas

}


function calcularPorcentagem(questao, totalRespostas) {
    let porcentagem = ((questao / totalRespostas) * 100).toFixed(2) 

    return porcentagem
}