var btnLogin = document.getElementById("idBtnLogin")
btnLogin.onclick = async function () {
    var usuario = document.getElementById("idUser").value
    var senha = document.getElementById("idPass").value

    var dadosLogin = {
        user: usuario,
        pass: senha
    }

    var options = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json'},
        body: JSON.stringify(dadosLogin)
    }

    var autorizacao = await fetch(`http://localhost:3002/login`, options)
    autorizacao = await autorizacao.json()

    localStorage.removeItem("token")
    localStorage.setItem("token", autorizacao.token)
    //console.log(autorizacao.token);
    

}


